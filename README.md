# Discord Game Bot
## Requirements
* Latest version of Ruby
* Latest version of discordrb
## Installation
1. Install Ruby
2. Install the discordrb gem using `gem install discordrb`
3. Create a Discord account if you don't already have one
4. Visit [here](https://discordapp.com/login?redirect_to=/developers/applications/me) to create a bot account
5. Copy the token from your bot account and paste it inside the .token file in the bot's directory
6. Run `ruby bot.rb` to start the bot
