
# Utility vector class for internal use
class Vector
  attr_accessor :x, :y
  def initialize(x, y)
    @x = x
    @y = y
  end

  def ==(vec)
    vec.instance_of?(Vector) && vec.x == @x && vec.y == @y
  end
end

DIRECTIONS = [
  Vector.new(0, 2),  # down
  Vector.new(2, 0),  # right
  Vector.new(0, -2), # up
  Vector.new(-2, 0)] # left

class Maze
  attr_reader :maze, :width, :height, :texture, :rand, :seed

  def initialize(width, height, texture = 0.80, seed = Random.new_seed)
    @width = width % 2 == 1 ? width : width + 1 # Enforce odd number
    @height = height % 2 == 1 ? height : height + 1 # Enforce odd number
    @width = @width < 3 ? 3 : @width
    @height = @height < 3 ? 3 : @height
    @maze = Array.new(@width) { Array.new(@height, :wall) }
    @texture = texture
    @rand = Random.new(seed)
    @seed = seed

    @maze[0][0] = :passage
    frontier = [[0, 0]]
    until frontier.empty?
      # Perform selection based on strategy
      if(@rand.rand > texture)                  # Select at random
        selected_index = @rand.rand(frontier.size)
      else                                      # Select newest cell
        selected_index = 0
      end

      selection = frontier[selected_index]
      selected_x = selection[0]
      selected_y = selection[1]
      unvisited = get_wall_neighbors(selected_x, selected_y)
      if unvisited.empty?
        frontier.delete_at(selected_index)
      else
        newest = unvisited[@rand.rand(unvisited.size)]
        new_x = newest[0]
        new_y = newest[1]
        @maze[new_x][new_y] = :passage
        @maze[(new_x + selected_x) / 2][(new_y + selected_y) / 2] = :passage
        frontier.insert(0, newest)
      end
    end
  end

  def get(x, y)
    if wall?(x, y)
      return :wall
    else
      return :passage
    end
  end

  def get_neighbors(x, y)
    neighbors = [[x, y - 2], [x, y + 2], [x + 2, y], [x - 2, y]]
    neighbors.select { |x, y| in_bounds?(x, y) }
  end

  def get_wall_neighbors(x, y)
    neighbors = get_neighbors(x, y)
    neighbors.select { |x, y| @maze[x][y] == :wall }
  end

  def in_bounds?(x, y)
    x >= 0 && x < @width && y >= 0 && y < @height
  end

  def wall?(x, y)
    (not in_bounds?(x, y)) || @maze[x][y] == :wall
  end

  def passage?(x, y)
    not wall?(x, y)
  end

  def dijkstra(startx, starty, goalx, goaly)
    # TODO: implement automatic solving functionality
    start = [startx, starty]
    queue = [start]
    distance = {start => 0}
    ancestor = {start => nil}
  end
end

class MazeGame
  def initialize(bot, channel, width, height, texture, seed, mode)
    @bot = bot
    @channel = channel
    @maze = Maze.new(width, height, texture, seed)
    @pos = Vector.new 0, 0
    @goal = Vector.new @maze.width - 1, @maze.height - 1
    @message = @channel.send(to_s)
    @left =  mode == "hack" ? "🇭" : "⬅"
    @down =  mode == "hack" ? "🇯" : "⬇"
    @up =    mode == "hack" ? "🇰" : "⬆"
    @right = mode == "hack" ? "🇱" : "➡"

    @message.react(@left)
    @message.react(@down)
    @message.react(@up)
    @message.react(@right)

    mode = "player" if mode == "auto" # disable auto for now
    if(mode == "auto")
      Thread.start do
        until @pos == @goal
          sleep 1
        end
      end
    elsif(mode == "player" || mode == "hack")
      @move = lambda { |event|
        if(event.message == @message)
          case event.emoji.name
          when @left ; move(:left)
          when @down ; move(:down)
          when @up ; move(:up)
          when @right ; move(:right)
          end

          print_maze

          if(@pos == @goal)
            finish
          end
        end
      }
      @add = bot.reaction_add({in: @channel}, &@move)
      @remove = bot.reaction_remove({in: @channel}, &@move)
    end
  end

  def move(direction)
    case direction
    when :left  ; @pos.x -= 1 if @maze.passage?(@pos.x - 1, @pos.y)
    when :down  ; @pos.y += 1 if @maze.passage?(@pos.x, @pos.y + 1)
    when :up    ; @pos.y -= 1 if @maze.passage?(@pos.x, @pos.y - 1)
    when :right ; @pos.x += 1 if @maze.passage?(@pos.x + 1, @pos.y)
    end
  end

  def finish
    @bot.remove_handler(@add)
    @bot.remove_handler(@remove)
    @message.react("🏁") # :checkered_flag:
    @message.delete_own_reaction(@left)
    @message.delete_own_reaction(@down)
    @message.delete_own_reaction(@up)
    @message.delete_own_reaction(@right)
  end

  def print_maze
    @message.edit(to_s)
  end

  def to_s
    s = "```Seed: #{@maze.seed}\r\nTexture: #{@maze.texture}\r\n"
    (-1..@maze.height).each do |y|
      (-1..@maze.width).each do |x|
        case @maze.get(x, y)
        when :wall ; s += '█'
        else
          if x == @pos.x && y == @pos.y
            s += '@'
          elsif Vector.new(x, y) == @goal
            s += '/'
          else
            s += ' '
          end
        end
      end
      s += "\r\n"
    end
    return s + "```"
  end
end
