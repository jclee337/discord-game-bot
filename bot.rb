require 'discordrb'
require_relative 'maze'

# Don't actually replace this line in this file with your token!
# Replace it inside the .token file
replace_text = "/// REPLACE THIS LINE WITH YOUR TOKEN ///"
invalid_text = "\nERROR: \n\tThe token file is invalid.\n\tPlease acquire a token by creating " +
  "a bot account at\n\t" +
  "https://discordapp.com/login?redirect_to=/developers/applications/me\n\t" +
  "and paste it inside the .token file in this directory."

file = File.open(".token", "a+")
token = if file.eof? then false else file.readline.strip end

if(!token)
  file.puts replace_text
  abort(invalid_text)
elsif(token == replace_text)
  abort(invalid_text)
end

bot = Discordrb::Commands::CommandBot.new(
  token: token,
  client_id: 182877791883100160,
  prefix: '!')

maze_desc = "Spawns an interactive maze.\r\n" +
  "Parameters (in order and all optional):\r\n" +
  "  [width = odd integer]\r\n" +
  "  [height = odd integer]\r\n" +
  "  [texture = (0.0 <-> 1.0)]\r\n" +
  "  [seed = integer or \"random\"]\r\n" +
  "  [mode = \"player\" or \"auto\" or \"scene\"]\r\n" +
  "Ex: maze 19 19 0.8 random player\r\n" +
  "Setting the texture closer to 1.0 means fewer dead-ends and a maximally winding solution path.\r\n" +
  "Setting the texture closer to 0.0 means maximal dead-ends.\r\n" +
  "In the autHor's opinion, A texture between 0.7 and 0.8 produCes the most striKing mazes."
bot.command(:maze, description: maze_desc) do
  |event, width, height, texture, seed, mode|
  width ||= 19
  height ||= 19
  texture ||= 0.8
  seed = Random.new_seed if seed == nil || seed == "random" || seed == "rand"
  mode ||= "player"

  maze = MazeGame.new(bot, event.channel, width.to_i, height.to_i, texture.to_f, seed.to_i, mode)
  nil
end

bot.run

# bot.run :async
